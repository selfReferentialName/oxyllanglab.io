---
layout: page
title: About
permalink: /about/
---

<div class="post-content">
    <h2 class="sname-main">Oxyl Language Website</h2>
<!--<p>Programming language designed as minimally as possible<br />while also allowing extreme power in diverse, non-turing complete environments.</p>-->
<p><br /></p>
<div class="manual manual-title">
<h3>
  Overview
  </h3></div>
<p>  <div class="manual-content">

Oxyl is a functional programming language focused on being explicit and safe but not clunky or verbose.
The language design is currently in the alpha stage (generally planned out, but the interface may change),
so any ideas or discussion about the language will be appreciated.

</div>
<p><br /></p>

  <div class="manual manual-title">
<h3>
  Using Oxyl
  </h3></div>
<p>  <div class="manual-content">

The <a href="https://gitlab.com/selfReferentialName/oxylc">compiler</a> is currently written in
<a href="https://ocaml.org/">OCaml</a> (though there are plans to become self hosting in the future),
but at this point in time, it isn't complete yet.

  </div>
