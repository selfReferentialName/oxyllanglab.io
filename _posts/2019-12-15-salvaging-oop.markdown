---
layout: page
title: "Slavaging Object Oriented Programming"
date:	2019-12-15 19:33:30 +04
---

<h2>Part One: What did OOP get wrong?</h2>

<p>
It is becoming increasingly apparent to an increasing number of programmers that
object oriented programming has failed. In order to see exactly why and how to fix
it, we must first understand <i>how</i> it failed.

<p>
First, what was object oriented programming trying to solve? Here are various
answers I've heard of (I have tried to select the ones that are actually correct here):

<ul>
<li>For the DRY principle of code reuse -- my CS1114 professor.</li>
<li>"Modularity" -- Robert Half&#174;</li>
<li>"Increases speed" -- I'm not citing this because that would embaress the source.</li>
<li>"Since the parallel development of classes is possible in OOP concept, It results in the quick development of the complete programmes." -- csetutor.com</li>
<li>Encapsulation -- Basically every proponent of OOP.</li>
<li>It mimics the real world -- Also most proponents of OOP, though this claim is dubious because software doesn't mimic the real world.</li>
</ul>

<p>
It's worth noting here that most of the meaningful ones came from a source that
gave both pros and cons to stuff.

<p>
Let's look at how OOP fares up on all of these:

<ul>
<li>DRY: It can do this, but it comes at the cost of a lot of boilerplate that negates it.</li>
<li>Modularity: While it can do this, it can sometimes require big graphs to see relations.</li>
<li>Parallel development: Sure, but this is a feature of modular design.</li>
<li>Encapsulation: OOP makes it very easy to break the principles of encapsulation if it is designed using OOP style encapsulation.</li>
<li>Real world mimicry: I absolutely hate it when I drop my OxylExpressionParser, don't you. Happens far too often.</li>
<li>Increases speed: Nope. It becomes much harder to do many simple and powerful optimizations. Increasing modularity splits up data and spreads it all over reducing cache speed.
Since stuff is spread across multiple files, stuff that could be a very simple and fast inlineing becomes a redundant call.
Stuff that just does stuff has to be put on the heap for some reason.</li>
</ul>

<p>
Another thing OOP does wrong is that certain programs can be similtaneously obfuscated and made idiomatic.
I once saw a network configuration script that just needs to call a few simple functions split across like five
different objects with all of the code being done in the constructors, split up with a thousand lines of comments.

<h2>Part Two: What did OOP provide that is useful?</h2>

<h3>Classes with members and methods</h3>

This is by far the most important thing in OOP. It is incredibly useful to use methods for stuff like
stdout.println "Hello world" instead of io/println stdout "Hello world". Operator overloading is also
incredibly nice to work with and only really costs complete type inference.

<h3>Public and private members and methods</h3>

Not quite as useful as other things. One interesting note is the members are private and always have getters and setters
idiom. This is dumb and can easily be done automatically. It begs the question of why writing unidiomatic stuff
is even allowed syntactically. Having a public/private distinction is useful though.

<h3>Dynamic dispatch</h3>

Sure. This can be done by extracting interfaces.

<h3>Subtype polymorphism</h3>

<p>
This one raises a lot philisophical questions during programming that causes premature abstraction.
Is a Chair a Furnature just like a Desk and a Table, or are Desks and Tables both ToppedFurnature,
and ToppedFurnature and Chairs both Furnature. Idioms say to add more classes, but now it turns out that
you can sit on a Chair and a Table, but a Desk has a bunch of stuff on it and you can't sit on it.
Now you need to change it so that Chairs and Tables are both Seats, with Seats and Desks both being Furnature.
Now you need to inline all the code from ToppedFurnature into Table and Desk.

<h2>Part Three: Making a better OOP subset.</h2>

<p>
Now that we know what works and what does'nt, what should we keep as is and what should be differentiated from data?

<h4>What to keep</h4>
<ul>
<li>Classes</li>
<li>Default to private stuff, add a pub keyword for public stuff</li>
<li>Limited inheritence with mixins</li>
</ul>

<h4>What to drop</h4>
<ul>
<li>Subtype polymorphism</li>
</ul>

<h2>Part Four: Extracting Encapsulation</h2>

By using the private public distinction at a file level, we can get encapsulation out of OOP and into procedural programming.
I'll call this use of OOP features at a module level with what would be called singletons in OOP "Method Oriented Programming".

<h2>Part Five: Extracting Reuse</h2>

<p>
For this, let's look at how functional programming does reuse. In an OOP language, in order to reuse
code about hashtables (without generics), one would have some base class HashTable with a virtual
function hash. In a functional language, one would need to feed a create function a key and a way to
hash said key, with typing done by system F. At first, it seems that inheritence and higher-level
functions are fundamentally different, but there is a similarity. Inheritence is just a function
from a type and a type to a type that simply merges everything.

<p>
How can we extend this? Instead of going from two types to one, we could go from a kind and values
(which is basically everything except kinds themselves) to a kind by adding functions based on the
values. This is <i>kind</i> of like OCaml functors.
