---
layout:	post
title:	Parsing Expressions With Pratt
date:	2019-10-27 07:51:35 +05
categories: ["internals", "parsing"]
---

<div class="post-content">
<p>
Order precedence can be really difficult to do.
In order to have good error messages, oxylc uses a custom built parser.
The algorithm for most stuff (which will probably get a post later) uses
parser combinators and can probably be summed up best as recursively
descending across various possibilities in linear order. The upside of
this is that the code can be <i>relaively</i> clean, but it does create
various interesting problems.
</p><p>
The sooner a rejection can be achieved, the faser the parser works.
(A rejecion meaning that the given code would be a synax error as
interpreted as whatever is being tested for)
If a rejection can be given in O(1) time, the parser will work in O(n) time,
but if a rejection takes the rest of the file to be achieved, the parser
could take as much as O(n^n) time.
Most of the language is designed in such a way that rejecions can be found in
one to three tokens.
</p><p>
Trying to do this to do precedence could lead to problems.
</p><p>
The first one is simply how to do it.
The only way I can think of doing it is to reject any interpretation that
has the wrong precedence and try again with it in a different precedence.
This produces a worst case parsing time of O((n*n!)^n), in addition to requiring
infinitely long lists (if the longest file possible has a length of a (which
is a countable infinity), the number of posible ways to interpret an expression
is a!). Seeing how I would like to have the compiler with less (to all you grammar
nazis out there, this is <i>very</i> uncountable, so I think that less works) than
aleph-(aleph-0) bytes of RAM, so this won't work.
</p><p>
The second one is the fact that it takes far, far too long.
</p><p>
Instead, we use the pratt algorithm (described in Vaughn Pratt's paper
<a href="https://web.archive.org/web/20151223215421/http://hall.org.ua/halls/wizzard/pdf/Vaughan.Pratt.TDOP.pdf">"Top Down Order Precedence"</a>).
</p><p>
An important thing to note before we get started is that this is for the expression
parser specifically, so it doesn't need to do as many things as in a full up language.
It's more akin to a 4-function calculator that needs to know when to stop.
</p><p>
<hr>
</p><p>
The algorithm is split into four functions: bp, nud, led, and parse.
In order to make it work without relying on global state, parse is split into two functions: parse and parse-r.
Here is a four function calculator in Oxyl-like pseudocode:
</p><p>
<div class=highlight>
<pre>
# Tok is a previously defined token type
# note that the switch statements don't have a default
# this would be a compile time error
tup Ret int, array{Tok}

int bp Tok t
	switch t.value & 0
		'+ -> 20
		'- -> 20
		'* -> 10
		'/ -> 10

Ret nud array{Tok} tl
	(t & 0).value, t ^ 0

Ret rec led array{Tok} tl int left
	int right, array{Tok} next = parse tl ^ 0 (bp tl & 0)
	switch (t & 0).value & 0
		'+ -> left + right, next
		'- -> left - right, next
		'* -> left * right, next
		'/ -> left / right, next

Ret rec parse-r array{Tok} tl int rbp int left
	if tl.size = 0
		left, tl
	elif rbp < bp tl & 0
		int left', array{Tok} tl' = led tl left
		parse-r tl' rbp left'
	else
		left, tl

Ret rec parse array{Tok} tl int rbp
	int left, array{Tok} tl' = nud tl
	parse-r tl' rbp left
</pre>
</div>
</p><p>
<hr>
</p><p>
Let's go over each function in order.
</p><p>
bp is a look up table that associates each operator with a "binding power."
</p><p>
nud gets the value of a token as an integer.
</p><p>
led is the first one where it gets interesting. In the first line, we see it calling
parse. What this line is doing is getting the expression to the right.
The rest of the lines are then preforming the operation.
</p><p>
parse-r (the naming convention being adding -r to the end of a function's recursive part)
tests when it makes sense to call led. At the end of the stream (in this example, it is
tl.size, but oxylc has a special Lex.Eof token), it simply reterns the token to the left.
If the token at the head of the list is able to overpower the token that called parse,
then it will run the operation, then continue the expression. Note that parse-r needs
to keep track of what is to the left.
</p><p>
parse will get the left as a terminal from nud and then call parse-r.
</p><p>
In practice, there is also an expression tl function that simply calls parse tl 0.
0 being a special binding power that will always keep accepting more values while possible.
</p><p>
<hr>
</p><p>
Lets look at what would happen if we ran this on, say (2+3*4+5).
<!--I apologise for anyone looking through this source code.-->
</p><p>
<ol>
 <li>parse 2+3*4+5 0 ➛ 19, []
   <ol>
   <li>nud 2+3*4+5 ➛ 2, +3*4+5</li>
   <li>parse-r +3*4+5 0 2 ➛ 19, []
     <ol>
     <li>led +3*4+5 2 ➛ 19, []
       <ol>
       <li>parse 3*4+5 20 ➛ 19, []
         <ol>
         <li>nud 3*4+5 ➛ 3, *4+5</li>
         <li>parse-r *4+5 20 3 ➛ 19, []
           <ol>
           <li>led *4+5 3 ➛ 12, +5
             <ol>
             <li>parse 4+5 10 ➛ 4, +5
               <ol>
               <li>nud 4+5 ➛ 4, +5</li>
               <li>parse-r +5 10 4 ➛ 4, +5</li>
               <!--</or></li>-->
             </ol></li>
           <li>parse-r +5 20 12 ➛ 19, []
             <ol>
             <li>led +5 12 ➛ 19, []
               <ol>
               <li>parse 5 20 ➛ 5, []
                 <ol>
                 <li>nud 5 ➛ 5, []</li>
                 <li>parse-r [] 20 5 ➛ 5, []</li>
                 </ol></li>
               </ol></li>
             <li>parse-r [] 20 19 ➛ 19, []</li>
             </ol></li>
           </ol></li>
         </ol></li>
      </ol></li>
      <li>parse-r [] 0 19 ➛ 19, []</li>
    </ol></li>
    </ol>
  </li>
</ol></li>
</ol>
</p><p>
<hr>
</p><p>
That's pretty much it. In oxylc, function calls are done by making identifiers check to
see if they are function calls or not by the first combinator, and there is a lot
of error checking I glossed over, but that is the gist of it.
</p><p>
Have fun writing parsers!
</p><p>
<a href="https://gitlab.com/selfReferentialName/oxylc/blob/ownparser/src/pratt.ml">Link to oxylc's implementation.</a>
</p>
</div>
