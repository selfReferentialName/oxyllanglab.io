---
layout:	page
title:	History and Parallelism
date:	2019-11-18 01:35:40 +04
categories: ["parallel", "gpu", "context", "parallination", "history"]
---

<h2>History</H2>

<p>
I started work on Oxyl to solve a problem:
current shading languages are ugly and hard to interpolate with CPU languages.

<p>
My idea was to create a language capable of generating code to work on both
CPU and GPU. The GPU is a very interesting environment to program in.
Compared with CPU, GLSL (the shading language I came from) has the following
restrictions:

<ul>
  <li>No recursion or mutual recursion.</li>
  <li>No dynamic allocation of memory.</li>
  <li>No pointers.</li>
  <li>Arrays must have static size.</li>
</ul>

<p>
The solution I came up with was to design the language in a way that it was
very easy and obvious to disable features to make it work in different, more
restrictive, non turing-complete environments. This is why Oxyl
1: requires recursive functions to be marked and
2: has two ways to mark recursive functions &mdash rec and tail.
tail acts like GLSL's while loop and can be used on GPU, while rec cannot.

<h2>Parallelism</h2>

<p>
Oxyl has two primitives for parallelism (though calling it two is a bit of
cheeting because one of them does multiple things and the other is actually
a class of things): contexts and parallination.

<h3>Contexts</h3>

<p>
A context started out as how to define what physical device code would run
on &mdash gpu (this may or may not be split into kernel and shader because
SPIR-V (the intended backend for GPGPU with Vulkan) makes them different)
or cpu. I later figured out that they could also be used to require
mutexes on all mutable globals. This is done my having subcontexts.

<h4>The serial Context</h4>

<p>
The serial context is the closest thing that Oxyl has to mutexes.
Each different subcontext is run completely serially. Because remote
context calling is done synchronously, calls to functions in serial
act as if they are done under a mutex.

<p>
The interesting thing about the serial context is that it is the only place
in the language that global mutable variables can be used (the closest thing
other contexts have is global constants, which are compile time constant).
This means that it is not only impossible to compile a program that has race
conditions related to multiple threads having access to a mutated variable
(which is also true in Rust), but it is even completely nonsensical.
Global variables don't exist without a mutex associated with them.

<p>
One interesting thing to research, language design wise, is allowing there
to be a self subcontext of serial on objects that ties the mutex to individual
objects. The benefit is more granular side effect control, but the cost is
enabling complex and messy ravioli code that doesn't compose.

<h3>Parallination</h3>

<p>
Parallination is the process in which different logical threads run the same
function on every element of an array. The methods of parallination are
called parallinators and differ in what they return. The primary parallinator
is the square parallinator and acts like OCaml's List.map function. It returns
the array of results from the parallinated function. The other parallinator
currently designed is the chevron parallinator and returns the first result.
This is intended to enable non-deterministic computation, but that is a
subject for a later post.

<p>
The way that parallination works is based on the fact that arrays have O(1)
indexing. This means that different threads are able to be recursively
called with ranges of indexes to make it so that each thread is able to run
with only O(log (base n / (number of OS threads)) n + (number of cores) / n)
of overhead. Theoretically, it would be able to work on B-trees of size
(m-(m+1)) with overhead O(log base m of n), but this would require B-trees
as a primitive, which would make the runtime much more complicated.
