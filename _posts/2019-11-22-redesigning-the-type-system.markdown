---
layout:		post
title:		Redesigning Oxyl's Type System
categories:	["types", "kinds", "hints"]
---

<img src="http://blog.oxyllang.org/banner.png">

<h2>Original Type System</h2>

When I first started working on Oxyl, I was intending to create a better
language than GLSL (among other things). In order to extend vector types,
I used dependent types (the vec type, which hasn't changed since). One
problem I had with GLSL was that you had to sprinkle stuff like float(intvar)
everywhere due to a lack of implicit conversions despite the language looking
a lot like C. I also used static typing to be more low level.

<h2>Compiler Design Problems</h2>

oxylc uses the following pipeline of representations of the program.

<ol>
<li>A filename.</li>
<li>An Input_stream.t</li>
<li>A string of the contents of the file</li>
<li>A list of characters</li>
<li>A list of tokens</li>
<li>An Ast.t Abstract Syntax Tree</li>
<li>Higher IR (Used to transform to ANF)</li>
<li>Verified IR (Naturally in ANF, intended have all conversions explicit)</li>
<li>LLVM IR</li>
<li>Whatever the heck LLVM codegen does wih it</li>
<li>An object file</li>
</ol>

The problem is that making conversions explicit in the VIR lowering pass is ugly
and running the pass on HIR (or VIR for that matter) leads to problems in
generaing the symbol table. Because of these problems, I looked into turning the
type system into a strongly typed system.

<h2>Theoretical Issues</h2>

<p>
The compiler cannot always know if two different types with the same kind are
equivalent (because they are possibly dependent on the result of the result of
a turing machine). This means that <i>something</i> must be called to do a
full typecheck for two types of the same kind.

<p>
Looking into this further I realised that the function call for runtime type
checking is much easier to work with than the function call for implicit type
conversion. The type conversion one will replace the original expression with
a method call on the original function. The problem is that type checking
requires the IR to be in ANF but the naiive implementation will need to
generate change expressions into a form that is not in ANF.

<img src="http://blog.oxyllang.org/data/redesigning-the-type-system/type-conversion.png">

<p>
With the way boh HIR is designed, it is very hard to change just one expression
(it might even take O(log n) time just to change one element, meaning running
it on every element will take O(n log n) time). Converting to ANF using
Anf.xform takes O(n log n) time, but can only be done on HIR. Outputing ANF
directly from an expression of m nodes takes O(m log n), so converting every
node when already in an expression will take O(n (log n) ^ 2).

<p>
In contrast, adding type check functions can just be done by adding more
expressions and eager evaluations, since type checking has no meaningful return
at runtime, and is only needed for the side effect of <i>gracefully and
explicitly exiting</i> (i.e. crashing) the program. Not only that, but adding
the eager evaluations is only dependent on the number of inputs to the
conversion function (which will usually be kept small because having to type
out Foo{1 2 3 4 5 "foo" 2.1 int string} multiple times is boring, monotonus,
ugly, and error prone. This also means that we don't have to do as many type
checks because a nice looking style will use as many primitive types as
possible. The time complexity of this by using joins for the symbol table with
n as the size of the function and m as the number of arguments to kinds is
O(n (m * log n) + n m).

<img src="http://blog.oxyllang.org/data/redesigning-the-type-system/type-check.png">

<h2>The New Type System</h2>

<p>
The result of this is that Oxyl now uses an explicit,
static, strong, dependent type system. Explicit in that every variable is
explicily given a type.
Static in that as much stuff related to the type system is done at compile time.
Strong in that there are no implicit conversions.
Dependent in that types are based on values that may change at runtime.
With being a dependent type system, the primitive of the type system is the
kind, which is like a function from values to a type (I say like because the
synax is different and, in the interest of simplicity, there are no higher
order kinds).

<p>
Another note is that type definitions (the type Foo = Bar statement) are
equivalent to the type they were defined as, but not other type definitions.

<h3>When is Type Checking Done?</h3>

Type checking is done as early as possible in order to keep generated code size
down and speed up. The simplest example is that if two types in the same funcion
are exactly the same in ANF, then they mus be the same (if things aren't being
overwritten, which is true in Oxyl if all loads are converted to function calls).
If literals are used, partial hits can be determined and remove some of the
runime cheking code, and partial misses can be determined to give a compile time
error (this one is important, because it means that vec{int 3} and vec{float 3}
can be differentiated at runtime, for example, as opposed to whenever the
function gets called, hopefully not in the middle of a critical saving function
for your production database). This one can actually be generalised into a
redundant call elimination pass on any variable with the right metadata set.

<h3>Hints</h3>

<p>
After reading
<a href="https://lexi-lambda.github.io/blog/2019/11/05/parse-don-t-validate/">
this blog post</a>, I had an idea for a way to extend the described thing in a
way that you can show multiple invariants on the same base type. The result is
the hint system.

<p>
Hints can be removed implicitly, but cannot be added implicitly. The intended
usage is to be able to notate various invariants to avoid needing to validate
them, and also to be able to use more efficiant methods. Additionally, hints
are not a feature of kinds, but can be added ad-hoc.

<p>
The way they are writen is as &#86;hint type.

<h4>Adding Hinted Methods</h4>

<p>
Hinted methods are methods that override the base mehods (or add methods).
They can be done like how C++ implements methods. This example shows the
function header for a dummy hinted method.

<p>
int (&#86;foo Bar).baz void

<h2>Needed Design Changes</h2>

<p>
Array indexing was originally designed on the assumpion that an array of
size 1 would be implicitly converted to cont. In order to replace this, two
methods are needed. The sizzle operator acts like before and uses ::, while
the normal index operator uses :.

<p>
Types can be converted using methods named the same as the target type.
This is accieved by making types and methods fall into different namespaces.

<p>
In order to enable the above is to use stuff like 12.i8, the way float literals
look needs to be changed. Previously "12.i8" was equal to "12.0 i8", but now it
is equal to "12 . i8".

<h3>A Note on the Noation of Integer Literals</h3>

<p>
C is a weakly typed language, but for some reason decides that integer literals
should have type int, which in C can be as little as unsigned 16 bits. If you want a
full 64 bit literal, you need to use 12l or 12L on Unix, but on Windows, it
must be long long int tmp = 12;. This can even lead to the incredible fact that
this function will return true.
<a href="https://stackoverflow.com/questions/14695118/2147483648-0-returns-true-in-c/14695202#14695202">
Source.</a>

<p>
bool is_negative_12_positive (void) {
	long long int foo = -2147483648;
	return foo > 0;
}

<p>
Rust unfortunately also makes the default type for integer literals 32 bit, but
at least it has static strong typing. A 64 bit literal looks like 12i64.

<p>
The problem I have with both of these forms is they get blocked weird. This
means that it doesn't look like a normal number to the average person. As a
result, it takes more time to look figure out what is going on. This is much
worse in Rust because rust uses type inference, so the fact that the i64
kind of looks like part of the number, so glancing over it could look like
a variable has type i32 instead of i64.

<p>
Also, a note on GLSL: by putting conversions at the start in function notation,
you put the less important parts of things before the more important things,
but when looking at most programming languages, I find that the main part of
information density in languages is just after the = sign in a line.
