---
layout:	post
title:	oxylc and Oxyl's Fundamental Problems
categories:	["compiler", "tool", "implementation", "theory"]
---

<img src="http://blog.oxyllang.org/banner.png"/>

<p>
Two days ago, I decided to try benchmarking oxylc to see how fast it is.
The lexer took 40 seconds to tokenise 5000 lines of code and had exponential
behavior (at least over the number of lines I was patient enough to test &em;
I think that asymptotically once you got to compilation time measured in days
it might smooth out to linear time). I needed to make it much, much faster
(like, 5 orders of magnitude &em; 100,000 faster). Fortunately, I am working
on a language designed to work on large swaths of data very quickly. I just
needed to add a few features so that it would be possible to implement the
lexer and I'd be good to go. I ran into a lot of problems. I've now decided
to begin the process of switching to a fully selfhosting compiler with
better implementation and some improved bits of language design. Let's look
at what needs to be fixed:

<h2>Implementation Problems</h2>

<h3>Overreliance on Linked Lists</h3>

<p>
This is the problem that started this whole ordeal. I wasn't really aware
of a way to get the tail of a sequence in O(1) time without breaking cache
coherency and introducing huge memory overhead. I thought that the O(1)
part was the most important and sprinkled linked lists over the code.
Doing this for the input to the lexer meant that it had to turn an byte array
with about 90,000 elements into a linked list, which I think added about
2300% memory overhead, which would require 90,000 allocations totalling
2.16 megabytes to store a 90 kilobyte file, which probably made the GC
very confused.

<p>
This one is easy to fix however. Pattern matching stuff like
<code>head::body</code> can be done in oxyl by saying something like<br/>
<code>cont head = sequence : 1<br/>
array{cont} body = sequence.splice 1 1.neg</code> (I might simplify that
next line to use an operator to get the tail via slicing e.g.
<code>sequence :<</code> or something). In OCaml, though, this change
is done by turning<br/><code>| head::body -></code> into <br/>
<code>| sequence -> (let head = Array.get sequence 0<br/>
let body = Array.slice sequence 1 0</code>.

<h3>Making the Body of Functions a Map</h3>

<p>
I'm not certain if this is a bad thing or not, but it is worth noting.

<p>
Most IRs in oxylc store the body as a map from variable names to values.
This makes it much harder to predict what will run first, even up to
the language design level. It does make it easy to preform variable
substitution.

<h3>Having Two Separate Internal Representations Need to Import</h3>

<p>
This one has been a huge pain. HIR and VIR both require type information
for every function. HIR uses it for TPM, TVM, and ANF genaration, while
VIR uses it for code generation and type checking. VIR has a map of all
methods a kind has, while HIR depends on name mangling. The result is that
a lot of builtin type information needs to be duplicated between Hir0 and
Llprims. This makes compiler development much, much slower.

<h3>Clashing With LLVM</h3>

<p>
This can basically be summed up by saying that vir has dependent types,
while LLVM doesn't. This muddies up the codegen code.

<h3>Position Information Gets Destroyed After Parsing</h3>

In order to keep the tuples small, I destroyed the position information
after parsing. I should probably keep it in until typechecking, and
a metadata system is probably a good way to do it.

<h2>Design Problems</h2>

<p>
A lot of the problems here are based on the theoretical roots and how
those roots were improperly made into low-level concepts.

<h3>Turning Types Into Values</h3>

<p>
Oxyl does parametric polymorphism by using first class types that
contain various bits of information (a string for the name, which
I'm not sure is necessary, size for storage in the own style which
roughly corresponds to unboxed types, and a function pointer to
the equality operator). The problem is that those pattern matched
types won't get automatically passed. Fixing this is important but
will messy up the implementation, and oxylc will require a huge rework
to get that done. Fortunately, a huge rework is planned.

<p>
An example of why this is an issue is the bind operator. In Haskell, if
you have a type operator (Haskell's name for a kind in Oxyl. Haskell
kinds are technically the type of type operators, which is similar to,
but not identical to, Oxyl kinds) t a (I think this is the right syntax.
Correct me if I'm wrong. I know in OCaml this is 'a t) which is a Monad,
you can bind it by just writing <code>input >>= function</code>, but
Oxyl needs to write <code>input.bind $function TypeOfInput</code> since
the function does not give its type. This is possible, since TPM creates
arguments for the types and types can be put into expressions,
but you need to know that it puts those in as arguments at the end.

<h3>Unboxable Types</h3>

<p>
Some types in oxyl are always passed by value (unboxable types), while
all other types are always passed by reference. That isn't much of a
problem because the native notion of reference does not cause mutability
(I'll probably do that with some special kind, say <code>ptr</code>
with methods like <code>ptr.set ptr.deref</code> to mutate stuff), but
it is when working with polymorphism. Polymorphism wants to work with
the same representation when passing stuff, but since <code>int</code>
is unboxable, we might get an invalid pointer that <i>looks</i> valid,
and since <code>vec</code> is unboxable, it might not fit in a single
register (or even be in a normal register at all; vec{int 4} will fit
a SIMD register fully).

<p>
Here is an example of what it looks like. Consider the following functions:<br/>
<code>int fooo int input<br/>
array{int} arrr array{int} input<br/>
float faaa float input<br/>
vec{i32 4} viii vec{i32 4} input</code><br/>
<code>fooo</code> and <code>arrr</code> will take and return in rax.
<code>viii</code> and <code>faaa</code> will take and return in xmm0.
The caller somehow needs to know what way to pass it in, and that cannot
be determined (for lack of information) by a polymorphic function if it
is public without putting the calling convention into the type information.

<h3>No Multiline Comments</h3>

<p>
Oxyl has no multiline comments. This is mostly because the syntax started
similar to Python, but there are places I feel like multiline comments
are a good thing. The problem is that I don't know what to mark them with.
The best thing would probably be something like <code>#< ... >#</code>.
