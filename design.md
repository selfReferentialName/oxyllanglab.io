---
layout: page
title: Design
permalink: /design/
---

<a href="https://drive.google.com/open?id=1lTQxh2yGXJDl4sSMEbKyTzuPZrE4AynQ">Here is a link to the documentation.</a>

<h2>The Abstract Machine</h2>

Just as C is based on the nonexistant machine of the modern SISD PDP11,
Oxyl is based on a nonexistant machine, which I'll call the
theoretically infinite threads. Technically, each context is one such
machine (except for serial, which is more like a modern single SIMD
core, but that is just a special case in which only one thread is used),
but this can be thought as an abstraction of a TITh machine that has AMP
(which is itself an abstraction of multiple finite SMP processing units
making up an AMP setup).
